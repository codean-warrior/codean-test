import java.util.Scanner;

public class CrudArray {
    private String[] data; // Array untuk menyimpan data
    private int size; // Jumlah data yang ada dalam array
    private static final int MAX_SIZE = 5; // Batas maksimum jumlah data yang bisa disimpan
    private Scanner scanner; // Objek Scanner untuk membaca input dari pengguna

    public CrudArray() {
        data = new String[MAX_SIZE]; // Inisialisasi array dengan ukuran MAX_SIZE
        size = 0; // Awalnya belum ada data dalam array, sehingga size di set 0
        scanner = new Scanner(System.in); // Inisialisasi objek Scanner
    }

    public void create() {
        if (size == MAX_SIZE) {
            System.out.println("Data sudah penuh. Tidak bisa menambahkan data baru.");
            return;
        }

        System.out.print("Masukkan data baru: ");
        String newData = scanner.next(); // Membaca data dari pengguna
        data[size] = newData; // Menyimpan data ke dalam array
        size++; // Menambahkan ukuran array setelah data ditambahkan
        System.out.println("Data berhasil ditambahkan.");
    }

    public void read() {
        if (size == 0) {
            System.out.println("Data kosong."); // Jika tidak ada data, cetak pesan ini
            return;
        }

        System.out.println("Data saat ini:");
        for (int i = 0; i < size; i++) {
            System.out.println(i + ". " + data[i]); // Menampilkan seluruh data yang ada dalam array
        }
    }

    public void update() {
        if (size == 0) {
            System.out.println("Data kosong. Tidak ada data yang bisa diubah.");
            return;
        }

        System.out.print("Masukkan indeks data yang ingin diubah: ");
        int index = scanner.nextInt(); // Membaca indeks data yang ingin diubah dari pengguna
        if (index < 0 || index >= size) {
            System.out.println("Indeks data tidak valid.");
            return;
        }

        System.out.print("Masukkan data baru: ");
        String newData = scanner.next(); // Membaca data baru yang akan menggantikan data lama
        data[index] = newData; // Mengganti data lama dengan data baru
        System.out.println("Data berhasil diubah.");
    }

    public void delete() {
        if (size == 0) {
            System.out.println("Data kosong. Tidak ada data yang bisa dihapus.");
            return;
        }

        System.out.print("Masukkan indeks data yang ingin dihapus: ");
        int index = scanner.nextInt(); // Membaca indeks data yang ingin dihapus dari pengguna
        if (index < 0 || index >= size) {
            System.out.println("Indeks data tidak valid.");
            return;
        }

        // Menggeser data ke kiri untuk menghapus data pada indeks tertentu
        for (int i = index; i < size - 1; i++) {
            data[i] = data[i + 1];
        }
        size--; // Mengurangi ukuran array setelah data dihapus
        System.out.println("Data berhasil dihapus.");
    }

    public static void main(String[] args) {
        CrudArray crud = new CrudArray(); // Membuat objek CrudArray
        Scanner scanner = new Scanner(System.in); // Objek Scanner untuk membaca input pengguna
        int choice;

        // Looping untuk menampilkan menu dan memproses input pengguna
        do {
            System.out.println("\nMenu:");
            System.out.println("1. Tambah Data");
            System.out.println("2. Lihat Data");
            System.out.println("3. Ubah Data");
            System.out.println("4. Hapus Data");
            System.out.println("5. Keluar");
            System.out.print("Pilih menu (1-5): ");
            choice = scanner.nextInt(); // Membaca pilihan menu dari pengguna

            switch (choice) {
                case 1:
                    crud.create(); // Memanggil metode create untuk menambahkan data
                    break;
                case 2:
                    crud.read(); // Memanggil metode read untuk menampilkan data
                    break;
                case 3:
                    crud.update(); // Memanggil metode update untuk mengubah data
                    break;
                case 4:
                    crud.delete(); // Memanggil metode delete untuk menghapus data
                    break;
                case 5:
                    System.out.println("Program selesai. Sampai jumpa!");
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih menu yang benar.");
            }
        } while (choice != 5);

        scanner.close(); // Menutup objek Scanner setelah program selesai
    }
}
